package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComputersAndTabletsPage extends BasePage {

    @FindBy(xpath = "//span[contains(text(),'Laptops ')]")
    private WebElement laptopsAndNetbooksButton;

    @FindBy(xpath = " //a[contains(text(),'Apple Laptops')]")
    private WebElement appleLaptopsButton;


    public ComputersAndTabletsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnLaptopsAndNetbooksButton() {
        laptopsAndNetbooksButton.click();
    }

    public void clickOnAppleLaptopsButton() {
        appleLaptopsButton.click();
    }
}