package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(xpath = "//input[contains(@placeholder,'Search')]")
    private WebElement searchInput;

    @FindBy(xpath = "//button[text()='Shop by category']")
    private WebElement shopByCategoryButton;

    @FindBy(xpath = "//a[contains(text(),'Computers & tablets')]")
    private WebElement computersAndTabletsButton;

    @FindBy(xpath = "//i[@id='gh-cart-n']")
    private WebElement amountOfProductsInCartPopup;

    @FindBy(xpath = "//span[@class='gh-ug-guest']//a[text()='Sign in']")
    private WebElement signInButton;


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void searchByKeyword(final String keyword) {
        searchInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickOnShopByCategoryButton() {
        shopByCategoryButton.click();
    }

    public void clickOnComputersAndTabletsButton() {
        computersAndTabletsButton.click();
    }

    public String getTextOfAmountProductsInCart() {
        return amountOfProductsInCartPopup.getText();
    }

    public WebElement getAmountOfProductsInCartPopup() {
        return amountOfProductsInCartPopup;
    }

    public void clickOnSignInButton() {
        signInButton.click();
    }
}
