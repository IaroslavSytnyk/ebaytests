package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AppleLaptopsPage extends BasePage {

    @FindBy(xpath = "//h3[contains(text(),'SSD (Mid 2015)')]")
    private WebElement macbookButton;

    @FindBy(xpath = "//input[@aria-label='32 GB']")
    private WebElement ramCheckbox;


    public AppleLaptopsPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnMacbookButton() {
        macbookButton.click();
    }

    public void clickOnRamCheckbox() {
        ramCheckbox.click();
    }

    public WebElement getRamCheckbox() {
        return ramCheckbox;
    }
}
