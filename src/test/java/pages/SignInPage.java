package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//span[@id='signin-reg-msg']")
    private WebElement signInForm;

    @FindBy(xpath = "//input[@id='userid']")
    private WebElement usernameInput;

    @FindBy(xpath = "//button[contains(@id,'continue')]")
    private WebElement continueButton;

    @FindBy(xpath = "//p[@id='signin-error-msg']")
    private WebElement errorMessagePopup;

    @FindBy(xpath = "//a[@id='create-account-link']")
    private WebElement createAccountButton;

    @FindBy(xpath = "//input[@name='email']")
    private WebElement emailInput;

    @FindBy(xpath = "//div[@id='email_w']")
    private WebElement wrongEmailMessagePopup;


    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getSignInForm() {
        return signInForm;
    }

    public void usernameInput(final String keyword) {
        usernameInput.sendKeys(keyword, Keys.ENTER);
    }

    public void clickOnContinueButton() {
        continueButton.click();
    }

    public WebElement getErrorMessagePopup() {
        return errorMessagePopup;
    }

    public void clickOnCreateAccountButton() {
        createAccountButton.click();
    }

    public void emailInput(final String keyword) {
        emailInput.sendKeys(keyword, Keys.TAB);
    }

    public WebElement getWrongEmailMessagePopup() {
        return wrongEmailMessagePopup;
    }
}
