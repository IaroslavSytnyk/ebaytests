package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MacbookPage extends BasePage {

    @FindBy(xpath = "//div[contains(@id,'tab-panel')]//a[contains(@class,'atc-link')]")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div[@class='item-action']//a[contains(@class,'primary')]")
    private WebElement buyItNowButton;

    @FindBy(xpath = "//a[contains(@class,'gh-cart-count')]")
    private WebElement viewInCartButton;


    public MacbookPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }

    public WebElement getAddToCartButton() {
        return addToCartButton;
    }

    public void clickOnBuyItNowButton() {
        buyItNowButton.click();
    }

    public WebElement getBuyItNowButton() {
        return buyItNowButton;
    }

    public void clickOnViewInCartButton() {
        viewInCartButton.click();
    }

    public WebElement getViewInCartButton() {
        return viewInCartButton;
    }
}
