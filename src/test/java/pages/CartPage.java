package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage {

    @FindBy(xpath = "//select[@class='listbox__control']")
    private WebElement quantityButton;

    @FindBy(xpath = "//select[@class='listbox__control']//option[@value='2']")
    private WebElement valueButton;

    @FindBy(xpath = "//div[@class='val-col total-row']")
    private WebElement totalPrice;

    @FindBy(xpath = "//span[contains(text(),'Items')]")
    private WebElement itemsAmount;

    @FindBy(xpath = "//button[@data-test-id='cart-remove-item']")
    private WebElement removeButton;

    @FindBy(xpath = "//span[contains(text(),'items ')]")
    private WebElement removeAlertPopup;


    public CartPage(WebDriver driver) {
        super(driver);
    }

    public void clickOnQuantityButton() {
        quantityButton.click();
    }

    public void clickOnValueButton() {
        valueButton.click();
    }

    public String getTotalPrice() {
        return totalPrice.getText();
    }

    public WebElement getItemsAmount() {
        return itemsAmount;
    }

    public void clickOnRemoveButton() {
        removeButton.click();
    }

    public WebElement getRemoveAlertPopup() {
        return removeAlertPopup;
    }
}
