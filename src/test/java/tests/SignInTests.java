package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SignInTests extends BaseTest {

    private String USERNAME = "Test_Epam";
    private String EMAIL = "WrongEmail@";

    @Test(priority = 1)
    public void checkLoginWithInvalidUsername() {
        getHomePage().clickOnSignInButton();
        getBasePage().implicitWait(30);
        getSignInPage().usernameInput(USERNAME);
        getSignInPage().clickOnContinueButton();
        assertTrue(getSignInPage().getErrorMessagePopup().isDisplayed());
    }

    @Test(priority = 2)
    public void checkRegistrationWithInvalidEmail() {
        getHomePage().clickOnSignInButton();
        getBasePage().implicitWait(30);
        getSignInPage().clickOnCreateAccountButton();
        getSignInPage().emailInput(EMAIL);
        getBasePage().waitForElementVisibility(15, getSignInPage().getWrongEmailMessagePopup());
        assertTrue(getSignInPage().getWrongEmailMessagePopup().isDisplayed());
    }
}
