package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PurchaseTests extends BaseTest {

    private String EXPECTED_AMOUNT_OF_GOODS_IN_CART = "1";
    private String EXPECTED_TOTAL_PRICE = "GBP 1,199.98";

    @Test(priority = 1)
    public void checkAddProductToCart() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnComputersAndTabletsButton();
        getComputersAndTabletsPage().clickOnLaptopsAndNetbooksButton();
        getComputersAndTabletsPage().clickOnAppleLaptopsButton();
        getBasePage().waitForPageLoadComplete(30);
        getAppleLaptopsPage().clickOnMacbookButton();
        getBasePage().waitForElementVisibility(20, getMacbookPage().getAddToCartButton());
        getMacbookPage().clickOnAddToCartButton();
        getBasePage().waitForElementVisibility(20, getHomePage().getAmountOfProductsInCartPopup());
        assertEquals(getHomePage().getTextOfAmountProductsInCart(), EXPECTED_AMOUNT_OF_GOODS_IN_CART);
    }

    @Test(priority = 2)
    public void checkBuyItNow() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnComputersAndTabletsButton();
        getComputersAndTabletsPage().clickOnLaptopsAndNetbooksButton();
        getComputersAndTabletsPage().clickOnAppleLaptopsButton();
        getAppleLaptopsPage().clickOnMacbookButton();
        getBasePage().waitForElementVisibility(20, getMacbookPage().getBuyItNowButton());
        getMacbookPage().clickOnBuyItNowButton();
        getBasePage().waitForPageLoadComplete(20);
        assertTrue(getSignInPage().getSignInForm().isDisplayed());
    }

    @Test(priority = 3)
    public void checkSumOfOrderWhenIncreaseGoodsQuantity() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnComputersAndTabletsButton();
        getComputersAndTabletsPage().clickOnLaptopsAndNetbooksButton();
        getComputersAndTabletsPage().clickOnAppleLaptopsButton();
        getBasePage().waitForPageLoadComplete(30);
        getAppleLaptopsPage().clickOnMacbookButton();
        getBasePage().waitForElementVisibility(20, getMacbookPage().getAddToCartButton());
        getMacbookPage().clickOnAddToCartButton();
        getBasePage().waitForElementVisibility(20, getMacbookPage().getViewInCartButton());
        getMacbookPage().clickOnViewInCartButton();
        getCartPage().clickOnQuantityButton();
        getCartPage().clickOnValueButton();
        getBasePage().waitForElementVisibility(20, getCartPage().getItemsAmount());
        assertEquals(getCartPage().getTotalPrice(), EXPECTED_TOTAL_PRICE);
    }

    @Test(priority = 4)
    public void checkRemoveFromCart() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnComputersAndTabletsButton();
        getComputersAndTabletsPage().clickOnLaptopsAndNetbooksButton();
        getComputersAndTabletsPage().clickOnAppleLaptopsButton();
        getBasePage().waitForPageLoadComplete(20);
        getAppleLaptopsPage().clickOnMacbookButton();
        getBasePage().waitForElementVisibility(15, getMacbookPage().getAddToCartButton());
        getMacbookPage().clickOnAddToCartButton();
        getBasePage().waitForElementVisibility(15, getMacbookPage().getViewInCartButton());
        getMacbookPage().clickOnViewInCartButton();
        getBasePage().waitForPageLoadComplete(20);
        getCartPage().clickOnRemoveButton();
        getBasePage().waitForElementVisibility(15, getCartPage().getRemoveAlertPopup());
        assertTrue(getCartPage().getRemoveAlertPopup().isDisplayed());
    }
}
