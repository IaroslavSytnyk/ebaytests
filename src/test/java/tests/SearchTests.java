package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends BaseTest {

    private String SEARCH_GOODS = "MacBook";
    private String RAM_SIZE = "32gb";
    private String RANDOM_CHARACTERS = "~#@!^%@";

    @Test(priority = 1)
    public void checkThatSearchResultsContainsSearchGoods() {
        getHomePage().searchByKeyword(SEARCH_GOODS);
        for (WebElement foundItem : getSearchResultsPage().getSearchResultsItemsList()) {
            assertTrue(foundItem.getText().contains(SEARCH_GOODS));
        }
    }

    @Test(priority = 2)
    public void checkItemsAmountDisplayedPerPage() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnComputersAndTabletsButton();
        getComputersAndTabletsPage().clickOnLaptopsAndNetbooksButton();
        getComputersAndTabletsPage().clickOnAppleLaptopsButton();
        getBasePage().waitForPageLoadComplete(20);
        assertEquals(getSearchResultsPage().getSearchResultsCount(), 48);
    }

    @Test(priority = 3)
    public void checkFilterByRAMSize() {
        getHomePage().clickOnShopByCategoryButton();
        getHomePage().clickOnComputersAndTabletsButton();
        getComputersAndTabletsPage().clickOnLaptopsAndNetbooksButton();
        getComputersAndTabletsPage().clickOnAppleLaptopsButton();
        getBasePage().waitForElementVisibility(30, getAppleLaptopsPage().getRamCheckbox());
        getAppleLaptopsPage().clickOnRamCheckbox();
        for (WebElement ramSize : getSearchResultsPage().getSearchResultsList()) {
            assertTrue(ramSize.getText().contains(RAM_SIZE));
        }
    }

    @Test(priority = 4)
    public void checkRandomCharactersInputInSearchField() {
        getHomePage().searchByKeyword(RANDOM_CHARACTERS);
        getBasePage().implicitWait(10);
        assertEquals(getSearchResultsPage().getSearchResultsCount(), 0);
    }
}